package dam.android.carlos.u5t9httpclient;

public class GeonamesPlace {

    private String summary;
    private String latitude;
    private String longitude;

    public GeonamesPlace(String summary, String longitude, String latitude) {
        this.summary = summary;
        this.latitude = latitude;
        this.longitude = longitude;
    }

    public String getSummary() {
        return summary;
    }

    public void setSummary(String summary) {
        this.summary = summary;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    @Override
    public String toString() {
        return "GeonamesPlace{" +
                "summary='" + summary + '\'' +
                ", latitude='" + latitude + '\'' +
                ", longitude='" + longitude + '\'' +
                '}';
    }
}
