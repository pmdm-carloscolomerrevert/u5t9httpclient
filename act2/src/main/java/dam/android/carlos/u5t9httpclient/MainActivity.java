package dam.android.carlos.u5t9httpclient;

import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.Network;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.inputmethod.InputMethodManager;
import android.widget.Adapter;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.lang.ref.WeakReference;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;

public class MainActivity extends AppCompatActivity implements OnClickListener {

    private final static String URL_GEONAMES = "http://api.geonames.org/wikipediaSearchJSON";
    private final static String URL_OPENWEATHERMAP = "http://api.openweathermap.org/data/2.5/weather?";
    private final static String USER_NAME = "carloscr";
    private final static String USER_OPENWEATHERMAP = "8126237b8396788c4915dff618f1d8a8";
    private final static String LANG = "es";
    private final static int ROWS = 10;

    private EditText etPlaceName;
    private Button btSearch;
    private ListView lvSearchResult;
    private ArrayList<String> listSearchResult;
    private GetHttpDataTask getHttpDataTask;
    private GetHttpDataOWM getHttpDataOWM;
    private static ArrayList<GeonamesPlace> geonamesPlaces;
    private static GeonamesPlace geonamePlace;
    private GeonamesPlace geonamesPlaceSeleccionado;
    private static Activity activity;
    private static ProgressDialog progressDialog;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        setUI();
    }

    private void setUI() {

        etPlaceName = findViewById(R.id.etPlaceName);
        btSearch = findViewById(R.id.btSearch);
        btSearch.setOnClickListener(this);
        geonamesPlaces = new ArrayList<>();
        listSearchResult = new ArrayList<>();
        activity = this;
        //TODO 2.1: Definimos el progressDialog y asignamos el texto que queremos que aparezca
        progressDialog = new ProgressDialog(this);
        //agregas un mensaje en el ProgressDialog
        progressDialog.setMessage("Cargando datos");

        lvSearchResult = findViewById(R.id.lvSeachResult);

        lvSearchResult.setAdapter(new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, listSearchResult));

        //TODO 1.3.1: guardamos el item selecionado en una variable
        lvSearchResult.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                geonamesPlaceSeleccionado = geonamesPlaces.get(position);
                Log.i("loge", geonamesPlaceSeleccionado.toString());
                try {
                    //TODO 1.3.2 creamos la url del openweathermap
                    URL url = new URL(URL_OPENWEATHERMAP + "lat=" + geonamesPlaceSeleccionado.getLatitude() + "&lon=" + geonamesPlaceSeleccionado.getLongitude()
                            + "&appid=" + USER_OPENWEATHERMAP);

                    getHttpDataOWM = new GetHttpDataOWM();
                    getHttpDataOWM.execute(url);
                } catch (MalformedURLException e) {
                    Log.i("loge", "MalformedURLException " + e.getMessage());
                }
            }

        });

    }


    @Override
    public void onClick(View v) {


        if (isNetworkAviable()) {
            String place = etPlaceName.getText().toString();

            if (!place.isEmpty()) {
                URL url;

                try {
                    //TODO 1.1.1: Modificamos el URL para que sea el resultado en español, y nos muestre la longitud y la latitud
                    url = new URL(URL_GEONAMES + "?q=" + place + "&maxRows=" + ROWS + "&lang=" + LANG + "&lat=47&lng=9&userName=" + USER_NAME);

                    // start new AsyncTask that will do all the search  work in a backgorund thread;
                    getHttpDataTask = new GetHttpDataTask(lvSearchResult);
                    getHttpDataTask.execute(url);

                    //TODO 1.2: Escondemos el teclado cuando pulsamos el boton de buscar.
                    InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(etPlaceName.getWindowToken(), 0);

                } catch (MalformedURLException e) {
                    Log.i("URL", e.getMessage());
                }
            } else {
                Toast.makeText(this, "Write a place to search", Toast.LENGTH_LONG).show();
            }
        } else {
            Toast.makeText(this, "Sorry, network is not available", Toast.LENGTH_LONG).show();
        }

    }


    // check if network is available and if there is connectivity

    public boolean isNetworkAviable() {
        boolean networkAviable = false;

        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);

        if (cm != null) {
            NetworkInfo networkInfo = cm.getActiveNetworkInfo();

            if (networkInfo != null && networkInfo.isConnected()) {
                networkAviable = true;

            }
        }
        return networkAviable;
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        if (getHttpDataTask != null) {
            getHttpDataTask.cancel(true);
            Log.e("onDestroy()", "ASYNCTASK was canceled");
        } else {
            Log.e("onDestroy()", "ASYNCTASK = NULL, was not canceled");
        }

    }

    private static class GetHttpDataTask extends AsyncTask<URL, Void, ArrayList<String>> {

        private final int CONNECTION_TIMEOUT = 15000;
        private final int READ_TIMEOUT = 1000;

        private final WeakReference<ListView> listViewWeakReference;

        public GetHttpDataTask(ListView listView) {
            this.listViewWeakReference = new WeakReference<>(listView);
            //TODO 2.2: Mostramos el progressDialog
            progressDialog.show();

        }

        @Override
        protected ArrayList<String> doInBackground(URL... urls) {

            HttpURLConnection urlConnection = null;
            ArrayList<String> searchResult = new ArrayList<>();

            try {
                urlConnection = (HttpURLConnection) urls[0].openConnection();

                urlConnection.setRequestProperty("Connection", "close");
                urlConnection.setConnectTimeout(CONNECTION_TIMEOUT);
                urlConnection.setReadTimeout(READ_TIMEOUT);


                if (urlConnection.getResponseCode() == HttpURLConnection.HTTP_OK) {

                    String resultStream = readStream(urlConnection.getInputStream());

                    JSONObject json = new JSONObject(resultStream);
                    JSONArray jArray = json.getJSONArray("geonames");

                    if (jArray.length() > 0) {

                        for (int i = 0; i < jArray.length(); i++) {
                            JSONObject item = jArray.getJSONObject(i);
                            //TODO 1.1.2: Añadimos al searchResult para que nos muestre la latitud y la longitud
                            searchResult.add(item.getString("summary") + " Longitud: " + item.getString("lng")
                                    + " Latitud: " + item.getString("lat"));

                            //TODO 1.1.3: Añadimos en el arraylist los items obtenidos
                            geonamePlace = new GeonamesPlace(item.getString("summary"), item.getString("lng"),
                                    item.getString("lat"));
                            geonamesPlaces.add(geonamePlace);

                            // Escape early if cancel() is called
                            if (isCancelled()) break;
                        }


                    } else {
                        searchResult.add("No information found");
                    }
                } else {
                    Log.i("URL", "ERROR CODE: " + urlConnection.getResponseCode());

                }
            } catch (IOException e) {
                Log.i("IOException", e.getMessage());
            } catch (JSONException e) {
                Log.i("JSONExeption", e.getMessage());
            } finally {
                if (urlConnection != null) {
                    urlConnection.disconnect();
                }
                //TODO 2.3: Ocultamos la barra de progreso cuando termina la tarea en background
                progressDialog.dismiss();
            }
            return searchResult;
        }

        @Override
        protected void onPostExecute(ArrayList<String> searchResult) {
            ListView listView = listViewWeakReference.get();

            if (listView != null) {
                if (searchResult != null && searchResult.size() > 0) {

                    ArrayAdapter<String> adapter = (ArrayAdapter<String>) listView.getAdapter();
                    adapter.clear();
                    adapter.addAll(searchResult);
                    adapter.notifyDataSetChanged();

                } else {
                    Toast.makeText(listView.getContext().getApplicationContext(), "Not possible to contact " +
                            URL_GEONAMES, Toast.LENGTH_LONG).show();
                }
            }

        }

        private String readStream(InputStream in) {
            StringBuilder sb = new StringBuilder();

            try {
                BufferedReader reader = new BufferedReader(new InputStreamReader(in, "UTF-8"));
                String nextLine = "";
                while ((nextLine = reader.readLine()) != null) {
                    sb.append(nextLine);
                }

            } catch (IOException e) {
                Log.i("IOException-ReadStram", e.getMessage());
            }

            return sb.toString();
        }

        @Override
        protected void onCancelled(ArrayList<String> strings) {
            super.onCancelled(strings);
            Log.e("onCancelled", "ASYNCTASK: I've   benn canceled and ready to GC clean");
        }
    }

    private static class GetHttpDataOWM extends AsyncTask<URL, Void, ArrayList<String>> {

        private final int CONNECTION_TIMEOUT = 15000;
        private final int READ_TIMEOUT = 1000;


        public GetHttpDataOWM() {
            //TODO 2.2: Mostramos el progressDialog
            progressDialog.show();

        }

        @Override
        protected ArrayList<String> doInBackground(URL... urls) {

            HttpURLConnection urlConnection = null;
            ArrayList<String> searchResult = new ArrayList<>();

            try {
                urlConnection = (HttpURLConnection) urls[0].openConnection();

                urlConnection.setRequestProperty("Connection", "close");
                urlConnection.setConnectTimeout(CONNECTION_TIMEOUT);
                urlConnection.setReadTimeout(READ_TIMEOUT);


                if (urlConnection.getResponseCode() == HttpURLConnection.HTTP_OK) {


                    String resultStream = readStream(urlConnection.getInputStream());
                    //TODO 1.3.3: obtenemos los datos necesarios
                    JSONObject json = new JSONObject(resultStream);
                    JSONArray jArray = json.getJSONArray("weather");
                    JSONObject jsonObject = json.getJSONObject("main");
                    JSONObject jsonObjectCoordinates = json.getJSONObject("coord");

                    if (jArray.length() > 0) {
                        Log.i("loge", "jArray.length >0 " + jArray.toString());

                        for (int i = 0; i < jArray.length(); i++) {
                            JSONObject item = jArray.getJSONObject(i);
                            //TODO 1.3.4: Añadimos al searchResult la informaciom que queremos mostrar
                            searchResult.add("Weather conditions for {" + jsonObjectCoordinates.getString("lon") + " " + jsonObjectCoordinates.getString("lat")
                                    + "}\nTEMP:" + jsonObject.getString("temp") + "ºC\nHUMIDITY: " + jsonObject.getString("humidity")
                                    + " % \n" + item.getString("description"));
                            Log.i("loge", "añadir " + searchResult.toString());

                            // Escape early if cancel() is called
                            if (isCancelled()) break;
                        }


                    } else {
                        searchResult.add("No information found");
                    }
                } else {
                    Log.i("URL", "ERROR CODE: " + urlConnection.getResponseCode());

                }
            } catch (IOException e) {
                Log.i("IOException", e.getMessage());
            } catch (JSONException e) {
                Log.i("JSONExeption", e.getMessage());
            } finally {
                if (urlConnection != null) {
                    urlConnection.disconnect();
                }
                //TODO 2.3: Ocultamos la barra de progreso cuando termina la tarea en background
                progressDialog.dismiss();
            }
            return searchResult;
        }

        @Override
        protected void onPostExecute(ArrayList<String> searchResult) {

            if (searchResult != null && searchResult.size() > 0) {
                // searchResult.get(0)
                //TODO 1.3.5: mostramos el toast con la información del item seleccionado
                Toast.makeText(activity.getApplicationContext(), searchResult.get(0), Toast.LENGTH_LONG).show();

                Log.i("loge", "onPostExecute: " + searchResult.size());


            }
        }


        private String readStream(InputStream in) {
            StringBuilder sb = new StringBuilder();

            try {
                BufferedReader reader = new BufferedReader(new InputStreamReader(in, "UTF-8"));
                String nextLine = "";
                while ((nextLine = reader.readLine()) != null) {
                    sb.append(nextLine);
                }

            } catch (IOException e) {
                Log.i("IOException-ReadStram", e.getMessage());
            }

            return sb.toString();
        }

        @Override
        protected void onCancelled(ArrayList<String> strings) {
            super.onCancelled(strings);
            Log.e("onCancelled", "ASYNCTASK: I've   benn canceled and ready to GC clean");
        }
    }

}
